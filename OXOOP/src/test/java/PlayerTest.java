/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author pc
 */
public class PlayerTest {

    public PlayerTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testName() {
        Player player = new Player();
        /*TestgetName*/
        char name = player.getName();
        System.out.println("Test Case Name = " + name);
        /*TestsetName*/
        player.setName('O');
        char newName = player.getName();
        System.out.println("Updated Test Case Name = " + newName);
    }

    @Test
    public void testgetWin() {
        Player o = new Player('O');
        Player x = new Player('X');
        Player player = new Player();
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
        if (player.getName() == 'O') {
            o.setWin();
            x.setLose();
        } else {
            x.setWin();
            o.setLose();
        }

        /*TestgetWin*/
        int win = player.getWin();
        System.out.println("Test Case Win = " + win);
    }

    public void testgetLose() {
        Player o = new Player('O');
        Player x = new Player('X');
        Player player = new Player();
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
        if (player.getName() == 'O') {
            o.setWin();
            x.setLose();
        } else {
            x.setWin();
            o.setLose();
        }

        /*TestgetLose*/
        int lose = player.getLose();
        System.out.println("Test Case Lose = " + lose);
    }

    public void testgetDraw() {
        Player o = new Player('O');
        Player x = new Player('X');
        Player player = new Player();
        Table table = new Table(o, x);
        int count = 0;
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);

        if (table.checkDraw() == true) {
            o.setDraw();
            x.setDraw();
            return;

        }

        /*TestgetDraw*/
        int draw = player.getDraw();
        System.out.println("Test Case Draw = " + draw);
    }

}
