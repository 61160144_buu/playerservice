
import java.util.Scanner;

public class Game {

    Scanner kb = new Scanner(System.in);
    private Table board;
    private Player o = new Player('O');
    private Player x = new Player('X');
    private int col;
    private int row;
    //int count;

    public Game() {
        col = 0;
        row = 0;
        //count = 0;

    }

    public void startGame() {
        board = new Table(o, x);

    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        for (int i = 0; i < board.getData().length; i++) {
            for (int j = 0; j < board.getData().length; j++) {
                System.out.print(" " + board.getData()[i][j] + " ");
            }
            System.out.println();

        }

    }

    public void showInput() {
        System.out.print("Please input row, col : ");
    }

    public boolean inputRowCol() {
        try {
            row = kb.nextInt();
            col = kb.nextInt();

            if (board.setRowCol(row, col) == true) {
                return true;

            } else {
                return false;
            }
        } catch (Exception e) {
            kb.nextLine();
            showWrongInput();
        }
        return false;

    }

    public boolean inputContinue() {
        System.out.print("Please inputPlayGame Y/N: ");
        String input = kb.next();
        if (input.equalsIgnoreCase("y")) {
            //count = 0;
            return true;
        } else if (input.equalsIgnoreCase("n")) {
            return false;
        } else {
            return true;
        }

    }

    public void showEnd() {
        System.out.println("End Game...");
    }

    public void checkScore(Player player) {
        if (player.getName() == 'O') {
            o.setWin();
            x.setLose();
        } else {
            x.setWin();
            o.setLose();
        }
    }

    public void runOnec() {

        while (true) {
            this.showTable();
            board.showTurn();
            //count++;
            this.showInput();
            if (this.inputRowCol() == false) {
                continue;
            }
            if (board.checkWin() == true) {
                Player player = board.getWinner();
                this.showTable();
                this.showWin(player);
                this.checkScore(player);

                return;

            } else if (board.checkDraw() == true) {
                checkDraw();
                return;

            }
            board.switchTurn();

        }

    }

    public void checkDraw() {
        o.setDraw();
        x.setDraw();
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.startGame();
            this.runOnec();
            if (!this.inputContinue()) {
                this.showScore();
                this.showEnd();
                return;
            }
            this.showScore();

        }

    }

    public void showWin(Player player) {
        System.out.println("Player " + player.getName() + " Win!!");
    }

    public void showScore() {
        System.out.println("O Win:" + o.getWin() + " Score" + " ," + "X Lose:" + x.getLose() + " Score");
        System.out.println("X Win:" + x.getWin() + " Score" + " ," + "O Lose:" + o.getLose() + " Score");
        System.out.println("O Draw:" + o.getDraw() + " Score" + " ," + "X Draw:" + x.getDraw() + " Score");

    }

    public void showWrongInput() {
        System.out.println("Wrong Position input again!");
    }

}
