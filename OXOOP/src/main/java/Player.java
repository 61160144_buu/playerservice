import java.io.Serializable;


public class Player implements Serializable{

    private char name;
    private int win;
    private int lose;
    private int draw;
    public Player() {
        name = 'o';
        win = 0;
        lose = 0;
        draw = 0;
    }

    @Override
    public String toString() {
        return  "{ win= " + win + ", lose= " + lose + ", draw= " + draw + '}';
    }

    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void setWin() {
        this.win++;
    }

    public void setLose() {
        this.lose++;
    }

    public void setDraw() {
        this.draw++;
    }
    

}
