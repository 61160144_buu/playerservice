
public class Table {

    private char[][] data = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o = new Player('O');
    private Player x = new Player('X');
    private Player win=null;
    int count =0;

    public Table(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.randomPlayer();

    }

    public boolean setRowCol(int row, int col) {
        if (data[row - 1][col - 1] == '-') {
            data[row - 1][col - 1] = currentPlayer.getName();
            count++;
            return true;
        } else {
            return false;
        }

    }
    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    


    public void switchTurn() {
        if (currentPlayer.getName() == 'X') {
            this.currentPlayer = o;

        } else {
            this.currentPlayer = x;

        }
    }

    public void showTurn() {
        System.out.println(currentPlayer.getName() + " Turn");

    }

    public void randomPlayer() {
        if (Math.round(Math.random()) == 0) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }

    }

    public Player getWinner() {
        return win;

    }

    public char[][] getData() {
        return data;
    }

    public boolean checkDraw() {
        if (count == 9) {
            showDraw();
            return true;
        }
        return false;
    }

    public void showDraw() {
        System.out.println("Play Draw!!");
    }

    public boolean checkWin() {
        if (checkRow(data) || checkCol(data) || checkX1(data) || checkX2(data)) {
        win = currentPlayer;
            return true;
        }
        return false;

    }

    public static boolean checkRow(char[][] board) {
        for (int row = 0; row < board.length; row++) {
            if (board[row][0] == board[row][1] && board[row][1] == board[row][2] && board[row][0] != '-') {
                return true;
            }
        }
        return false;
    }

    public static boolean checkCol(char[][] board) {
        for (int col = 0; col < board[0].length; col++) {
            if (board[0][col] == board[1][col] && board[1][col] == board[2][col] && board[0][col] != '-') {
                return true;
            }
        }
        return false;
    }

    public static boolean checkX1(char[][] board) {

        if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-')) {
            return true;
        }
        return false;
    }

    public static boolean checkX2(char[][] board) {
        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != '-') {
            return true;
        }
        return false;
    }

}
